import unittest
from Figuras import Triangulo as t

class Test_triangle(unittest.TestCase):
    Prueba = None
    Base = 0
    Altura = 0
    def setUp(self) -> None:
        self.Prueba = t.Triangulo()
        self.Base = 10
        self.Altura = 5

    def test_instance(self):
        self.assertNotEqual(self.Prueba, None)

    def test_triangle_area(self):
        self.assertEqual(round(self.Prueba.area_t(self.Base, self.Altura )), 25)

    def test_triangle_per(self):
        self.assertEqual(round(self.Prueba.peri_t(self.Altura)), 15)


if __name__ == '__main__':
    unittest.main()