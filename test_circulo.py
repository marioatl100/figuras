import unittest
from Figuras import Circulo as c


class Test_circle(unittest.TestCase):
    Prueba = None
    Radio = 10
    def setUp(self) -> None:
        self.Prueba = c.Circulo()
        self.radio = 10

    def test_instance(self):
        self.assertNotEqual(self.Prueba, None)

    def test_circle_area(self):
        self.assertEqual(round(self.Prueba.area_circulo(self.Radio)), 314)

    def test_circle_per(self):
        self.assertEqual(round(self.Prueba.peri_circulo(self.Radio)), 63)


if __name__ == '__main__':
    unittest.main()