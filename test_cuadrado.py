import unittest
from Figuras import Cuadrado as c

class Test_square(unittest.TestCase):
    Prueba = None
    Lado = 0
    def setUp(self) -> None:
        self.Prueba = c.Cuadrado()
        self.Lado = 10

    def test_instance(self):
        self.assertNotEqual(self.Prueba, None)

    def test_square_area(self):
        self.assertEqual(round(self.Prueba.area_c(self.Lado)), 100)

    def test_square_per(self):
        self.assertEqual(round(self.Prueba.peri_c(self.Lado)), 40)


if __name__ == '__main__':
    unittest.main()